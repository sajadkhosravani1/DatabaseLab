# Database lab
In this repository i'm going to Share Database lab experiences with you.

Note:
- The DBMS i used is **MySQL (Version 8.0)**. 
- The Operating system i used is **Linux** and i use **bash** commands to progress the experiences.

## Install and use MySQL in bash
Use [this page](https://www.digitalocean.com/community/tutorials/how-to-install-mysql-on-ubuntu-18-04).

## experiences
- [experience 1](./exp1): Creating table
- [experience 2](./exp2): Constraints and alter
